import MockRestService from '@/services/MockRestService'

const state = {
  statusesList: []
}

const getters = {
  STATUSES: state => {
    return state.statusesList;
  },
  GET_STATUSE: state => id => {
    return state.statusesList.find(status => status.id === id);
  }
}

const mutations = {
  SET_STATUSES: (state, statusesList) => {
    state.statusesList = statusesList;
  },
  ADD_STATUSE: (state, newStatus) => {
    state.statusesList.push(newStatus);
  },
  EDIT_STATUSE: (state, editedStatus) => {
    state.statusesList = state.statusesList.map(status => {
      if (status.id === editedStatus.id) {
        return Object.assign({}, status, editedStatus)
      }
      return status
    })
  }
}
const actions = {
  GET_STATUSES: (context) => {
    MockRestService.getStatuses().then((list) => {
      context.commit('SET_STATUSES', list);
    })
  },
  DELETE_STATUSES: (context, deleteStatusesArray) => {
    MockRestService.deleteStatuses(deleteStatusesArray).then((list) => {
      context.commit('SET_STATUSES', list);
    })
  },
  SAVE_NEW_STATUSE: (context, newStatus) => {
    MockRestService.addStatus(newStatus).then(() => {
      context.commit('ADD_STATUSE', newStatus);
    })
  },
  SAVE_EDITED_STATUSE: (context, editedStatus) => {
    MockRestService.editStatus(editedStatus).then(() => {
      context.commit('EDIT_STATUSE', editedStatus);
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}