import MockRestService from '@/services/MockRestService'

const state = {
  notesList: []
}

const getters = {
  NOTES: state => {
    return state.notesList;
  },
  GET_NOTE: state => id => {
    return state.notesList.find(note => note.id === id);
  }
}

const mutations = {
  SET_NOTES: (state, notesList) => {
    state.notesList = notesList;
  },

  ADD_NOTE: (state, newNote) => {
    state.notesList.push(newNote);
  },

  EDIT_NOTE: (state, editedNote) => {
    state.notesList = state.notesList.map(note => {
      if (note.id === editedNote.id) {
        return Object.assign({}, note, editedNote)
      }
      return note
    })
  }
}

const actions = {
  GET_NOTES: (context) => {
    MockRestService.getNotes().then((list) => {
      context.commit('SET_NOTES', list);
    })
  },
  SAVE_NEW_NOTE: (context, newNote) => {
    MockRestService.addNote(newNote).then(() => {
      context.commit('ADD_NOTE', newNote);
    })
  },
  DELETE_NOTES: (context, deleteNotesArray) => {
    MockRestService.deleteNotes(deleteNotesArray).then((list) => {
      context.commit('SET_NOTES', list);
    })
  },
  SAVE_EDITED_NOTE: (context, editedNote) => {
    MockRestService.editNote(editedNote).then(() => {
      context.commit('EDIT_NOTE', editedNote);
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}