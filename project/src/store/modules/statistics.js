import MockRestService from '@/services/MockRestService'

const state = {
  statisticsList: []
}

const getters = {
  STATISTICS: state => {
    return state.statisticsList;
  },
}

const mutations = {
  SET_STATISTICS: (state, statisticsList) => {
    state.statisticsList = statisticsList;
  }
}

const actions = {
  GET_STATISTICS: (context) => {
    MockRestService.getStatistics().then((list) => {
      context.commit('SET_STATISTICS', list);
    })
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}