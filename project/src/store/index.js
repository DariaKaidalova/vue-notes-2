import Vue from 'vue'
import Vuex from 'vuex'
import notes from './modules/notes'
import statistics from './modules/statistics'
import statuses from './modules/statuses'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    notes,
    statistics,
    statuses
  }
})