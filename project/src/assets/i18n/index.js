import en from './en.json'
import pl from './pl.json'

export const defaultLanguage = 'en'
export const plLanguage = 'pl'

export const languages = {
  en: en,
  pl: pl,
}