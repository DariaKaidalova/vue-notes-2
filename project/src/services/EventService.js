export default {

  addNote(newNote, notesList) {
    if(newNote.id === '') {
      const firstId = notesList[0] ? notesList[0].id : 0;
      const nextId = notesList.reduce((max, next) => Math.max(max, next.id), firstId) + 1;
      newNote.id = nextId;
    }
    notesList.push(newNote);
    return notesList;
  },

  getNote(noteId, notesList) {
    for(let i = 0; i < notesList.length; i++) {
      if(notesList[i].id === noteId) {
        return notesList[i];
      }
    }
    return {}
  },

  editNote(noteItem, notesList) {
    for(let i = 0; i < notesList.length; i++) {
      if(notesList[i].id === noteItem.id) {
        notesList[i] = noteItem;
        return notesList;
      }
    }
    return notesList;
  },

  deleteNotes(deleteNotesArray, notesList) {
    return notesList.filter(note => !deleteNotesArray.includes(note.id));
  },

  calculateStatistics(notesList, statusesList) {
    const statisticsList = []
    for(let i = 0; i < statusesList.length; i++) {
      const statisticsItem = {
        id: statusesList[i].id,
        title: statusesList[i].title,
        value: 0
      }
      statisticsList.push(statisticsItem);
    }
    for(let i = 0; i < notesList.length; i++) {
      for(let j = 0; j < statisticsList.length; j++) {
        if(notesList[i].status.id == statisticsList[j].id) {
          statisticsList[j].value++;
        }
      }
    }
    return statisticsList;
  },

  deleteStatuses(deleteStatusesArray, statusesList) {
    return statusesList.filter(status => !deleteStatusesArray.includes(status.id));
  },

  addStatus(newStatus, statusesList) {
    if(newStatus.id === '') {
      const firstId = statusesList[0] ? statusesList[0].id : 0;
      const nextId = statusesList.reduce((max, next) => Math.max(max, next.id), firstId) + 1;
      newStatus.id = nextId;
    }
    statusesList.push(newStatus);
    return statusesList;
  },

  getStatus(statusId, statusesList) {
    for(let i = 0; i < statusesList.length; i++) {
      if(statusesList[i].id === statusId) {
        return statusesList[i];
      }
    }
    return {}
  },

  editStatus(statusItem, statusesList, notesList) {
    for(let i = 0; i < statusesList.length; i++) {
      if(statusesList[i].id === statusItem.id) {
        statusesList[i] = statusItem;
        notesList = this.editNotesStatuses(statusItem, notesList)
        return { statusesList, notesList} ;
      }
    }
    return { statusesList, notesList};
  },

  editNotesStatuses(statusItem, notesList) {
    for(let i = 0; i < notesList.length; i++) {
      if(notesList[i].status.id === statusItem.id) {
        notesList[i].status = statusItem;
        return notesList;
      }
    }
    return notesList;
  },

  checkStatusTitle(status, statusesList) {
    for(let i = 0; i < statusesList.length; i++) {
      if(statusesList[i].title.toLowerCase() === status.title.toLowerCase() && status.id !== statusesList[i].id) {
        return {
          error: true, 
          message: `Status ${status.title} have already exists`
        };
      }
    }
    return {
      error: false,
      message: `Status ${status.title} can be added`
    };
  },

  checkUsedStatuses(notesList, statusesList) {
    let usedStatusesArray = [];
    for(let i = 0; i < statusesList.length; i++) {
      for(let j = 0; j < notesList.length; j++) {
        let statusId = statusesList[i].id;
        let noteStatusId = notesList[j].status.id;
        if(statusId == noteStatusId && !usedStatusesArray.includes(statusId)) {
          usedStatusesArray.push(statusId);
        }
      }
    }
    return usedStatusesArray;
  }
}
