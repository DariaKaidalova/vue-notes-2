import { defaultLanguage } from '@/assets/i18n/index.js'
const languageKey = 'language';

export default {

  setLocalStorageLanguage(value) {
    window.localStorage.removeItem(languageKey);
    window.localStorage.setItem(languageKey, value);
  },

  getLocalStorageLanguage() {
    if (!window.localStorage.getItem(languageKey)) {
      window.localStorage.setItem(languageKey, defaultLanguage);
    }
    let language = window.localStorage.getItem(languageKey);
    return language;
  }
}
