import dbFile from '@/assets/db/db.json'
import EventService from '@/services/EventService'

export default {

  setLocalStorageItems (itemsTitle, items) {
    window.localStorage.removeItem(itemsTitle);
    window.localStorage.setItem(itemsTitle, JSON.stringify(items));
  },

  getLocalStorageItems (itemsTitle) {
    return JSON.parse(window.localStorage.getItem(itemsTitle))
  },

  /* notes */
  getNotes() {
    return new Promise((resolve) => {
      if (!this.getLocalStorageItems('notesList')) {
        this.setLocalStorageItems('notesList', dbFile.notesList);
      }
      let notesList = this.getLocalStorageItems('notesList');
      resolve(notesList);
    });
  },

  addNote(newNote) {
    return new Promise((resolve) => {
      let notesList = this.getLocalStorageItems('notesList');
      let updatedNotesList = EventService.addNote(newNote, notesList);
      this.setLocalStorageItems('notesList', updatedNotesList);
      resolve();
    });
  },

  getNote(noteId) {
    return new Promise((resolve) => {
      let notesList = this.getLocalStorageItems('notesList');
      let noteItem = EventService.getNote(noteId, notesList);
      resolve(noteItem);
    });
  },

  editNote(editedNote) {
    return new Promise((resolve) => {
      let notesList = this.getLocalStorageItems('notesList');
      let updatedNotesList = EventService.editNote(editedNote, notesList);
      this.setLocalStorageItems('notesList', updatedNotesList);
      resolve();
    });
  },

  deleteNotes(deleteNotesArray) {
    return new Promise((resolve) => {
      let notesList = this.getLocalStorageItems('notesList');
      let updatedNotesList = EventService.deleteNotes(deleteNotesArray, notesList);
      this.setLocalStorageItems('notesList', updatedNotesList);
      resolve(updatedNotesList);
    });
  },

  /* statuses */
  getStatuses() {
    return new Promise((resolve) => {
      if (!this.getLocalStorageItems('statusesList')) {
        this.setLocalStorageItems('statusesList', dbFile.statusesList);
      }
      let statusesList = this.getLocalStorageItems('statusesList');
      resolve(statusesList);
    });
  },

  deleteStatuses(deleteStatusesArray) {
    return new Promise((resolve) => {
      let statusesList = this.getLocalStorageItems('statusesList');
      let updatedStatusesList = EventService.deleteStatuses(deleteStatusesArray, statusesList);
      this.setLocalStorageItems('statusesList', updatedStatusesList);
      resolve(updatedStatusesList);
    });
  },

  addStatus(newStatus) {
    return new Promise((resolve) => {
      let statusesList = this.getLocalStorageItems('statusesList');
      let updatedStatusesList = EventService.addStatus(newStatus, statusesList);
      this.setLocalStorageItems('statusesList',updatedStatusesList);
      resolve(updatedStatusesList);
    });
  },

  getStatus(statusId) {
    return new Promise((resolve) => {
      let statusesList = this.getLocalStorageItems('statusesList');
      let statusItem = EventService.getStatus(statusId, statusesList);
      resolve(statusItem);
    });
  },


  editStatus(editedStatus) {
    return new Promise((resolve) => {
      let statusesList = this.getLocalStorageItems('statusesList');
      let notesList = this.getLocalStorageItems('notesList');
      let updatedList = EventService.editStatus(editedStatus, statusesList, notesList);
      this.setLocalStorageItems('statusesList', updatedList.statusesList);
      this.setLocalStorageItems('notesList', updatedList.notesList);
      resolve();
    });
  },

  checkStatusTitle(status) {
    return new Promise((resolve) => {
      let statusesList = this.getLocalStorageItems('statusesList');
      let checkResult = EventService.checkStatusTitle(status, statusesList);
      resolve(checkResult);
    });
  },

  checkUsedStatuses() {
    return new Promise((resolve) => {
      let notesList = this.getLocalStorageItems('notesList');
      let statusesList = this.getLocalStorageItems('statusesList');
      let usedStatusesArray = EventService.checkUsedStatuses(notesList, statusesList);
      resolve(usedStatusesArray);
    });
  },

  /* statistics */
  getStatistics() {
    return new Promise((resolve) => {
      this.getNotes();
      this.getStatuses();
      let notesList = this.getLocalStorageItems('notesList');
      let statusesList = this.getLocalStorageItems('statusesList');
      let statisticsList = EventService.calculateStatistics(notesList, statusesList);
      console.log(statisticsList);
      resolve(statisticsList);
    });
  }
}
