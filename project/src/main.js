import Vue from 'vue'
import App from './App.vue'
import router from './router'
// validation
import Vuelidate from 'vuelidate'
// font awesome 
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSort, faSortUp, faSortDown, faExclamationTriangle, faEdit } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
// vue select
import vSelect from 'vue-select'
// vuex
import {store} from './store';
// vue table 2
import {ClientTable} from 'vue-tables-2';
// bootstrap
import BootstrapVue from 'bootstrap-vue'
// internationalization plugin
import VueI18n from 'vue-i18n'
import { languages } from './assets/i18n/index.js'
import { defaultLanguage, plLanguage } from './assets/i18n/index.js'

// validation
Vue.use(Vuelidate)
// font awesome 
library.add(faSort, faSortUp, faSortDown, faExclamationTriangle, faEdit)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false
Vue.config.productionTip = false
// vue select
Vue.component('v-select', vSelect)
// vue table 2
Vue.use(ClientTable, {}, false, 'bootstrap4');
// bootstrap
Vue.use(BootstrapVue)
// internationalization plugin
Vue.use(VueI18n)

const messages = Object.assign(languages);
const i18n = new VueI18n({
  locale: defaultLanguage,
  fallbackLocale: plLanguage,
  messages
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
