export const addMixin = {
  data() {
    return {
      isShowDialog: false
    }
  },
  methods: {
    openAddDialog() {
      this.isShowDialog = true;
    },

    onSubmitAddAction(newItem) {
      this.isShowDialog = false;
      this.onAddAction(newItem);
    },
    
    onCancelAddAction() {
      this.isShowDialog = false;
    }
  }
}