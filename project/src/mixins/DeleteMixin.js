export const deleteMixin = {
  data() {
    return {
      isShowDeleteToast: false,
      deleteArray: [],
      usedArray: [],
      isCheckedAll: false
    }
  },
  methods: {
    onSubmitDeleteAction() {
      this.onDeleteAction(this.deleteArray);
      this.isShowDeleteToast = false;
      this.isCheckedAll = false;
    },

    onCancelDeleteAction() {
      this.deleteArray = [];
      this.isShowDeleteToast = false;
      this.isCheckedAll = false;
    },

    onDeleteItem(id) {
      let isChecked = !this.deleteArray.includes(id);
      if(isChecked) {
        this.deleteArray.push(id);
      } else {
        for(let i = 0; i < this.deleteArray.length; i++) {
          if(this.deleteArray[i] === id) {
            this.deleteArray.splice(i, 1);
          }
        }
      }
      this.toggleToast();
    },

    toggleCheckedAll() {
      this.isCheckedAll = !this.isCheckedAll;
      this.deleteArray = [];
      if (this.isCheckedAll) {
        for (let i in this.list) {
          if(!this.usedArray.includes(this.list[i].id)) {
            this.deleteArray.push(this.list[i].id);
          }
        }
      }
      this.toggleToast();
    },

    toggleToast() {
      if (this.deleteArray.length > 0) {
        this.isShowDeleteToast = true;
      } else {
        this.isShowDeleteToast = false;
        this.isCheckedAll = false;
      }
    },

    disableMainCheckbox() {
      return this.usedArray.length === this.list.length
    }
  }
}