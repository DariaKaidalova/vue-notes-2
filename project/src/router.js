import Vue from 'vue'
import Router from 'vue-router'
import NotesPage from '@/components/NotesPage'
import NotesEditPage from '@/components/NotesEditPage'
import StatusesPage from '@/components/StatusesPage'
import StatusesEditPage from '@/components/StatusesEditPage'
import StatisticsPage from '@/components/StatisticsPage'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'NotesPage',
      component: NotesPage
    },
    {
      path: '/edit/:id',
      name: 'NotesEditPage',
      component: NotesEditPage
    },
    {
      path: '/statuses',
      name: 'StatusesPage',
      component: StatusesPage
    },
    {
      path: '/statuses-edit/:id',
      name: 'StatusesEditPage',
      component: StatusesEditPage
    },
    {
      path: '/statistics',
      name: 'StatisticsPage',
      component: StatisticsPage
    }
  ]
})
