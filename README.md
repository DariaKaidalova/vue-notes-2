# Test task

## Project structure

### The project consist of the following parts:
- 'project' folder with vue-cli

## About project

### How to start vue project
- Go into the 'project' folder 
- Install it using `npm install` command
- Start project using `npm run serve`

### Vue project structure
- Styles:
I used sass for styling. They are located in the folder 'src/assets/styles'.
- DB file:
DB file with initial mock data lies in the folder 'src/assets/db'. I used localStorage as a mock server.
- Internalization:
Internalization i18n plugin settings are located in the folder 'src/assets/i18n'.
- Components:
I created components in the folder 'src/components'.
- Services: 
I created mock rest service ('MockRestService.js'), event service ('EventService.js') and language service (LanguageService.js) in the folder 'src/services'.
- VueX: 
State management VueX library files lies in the folder 'src/store'.
- Vue mixins:
Vue mixins files lies in the folder 'src/mixins'.